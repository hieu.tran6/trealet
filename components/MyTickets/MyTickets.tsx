import { Stack } from "@chakra-ui/react";
import { TicketItem } from "./TicketItem";
import { ticketData } from "./_data";

export const MyTickets: React.FunctionComponent = () => {
  return (
    <Stack spacing="6">
      {ticketData.map((item) => (
        <TicketItem key={item.id} {...item} />
      ))}
    </Stack>
  );
};
