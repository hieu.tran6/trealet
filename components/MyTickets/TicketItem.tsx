import {
  CloseButton,
  Flex,
  Link,
  Select,
  SelectProps,
  useColorModeValue,
} from "@chakra-ui/react";
import * as React from "react";
import { PriceTag } from "../MyAuctions/PriceTag";
import { TicketProductMeta } from "./TicketProductMeta";

type CartItemProps = {
  isGiftWrapping?: boolean;
  name: string;
  description: string;
  quantity: number;
  price: number;
  currency: string;
  imageUrl: string;
  onChangeQuantity?: (quantity: number) => void;
  onClickGiftWrapping?: () => void;
  onClickDelete?: () => void;
};

const QuantitySelect = (props: SelectProps) => {
  return (
    <Select
      maxW="64px"
      aria-label="Select quantity"
      focusBorderColor={useColorModeValue("blue.500", "blue.200")}
      {...props}
    >
      <option value="1">1</option>
      <option value="2">2</option>
      <option value="3">3</option>
      <option value="4">4</option>
    </Select>
  );
};

export const TicketItem = (props: CartItemProps) => {
  const {
    name,
    description,
    imageUrl,
    currency,
    price,
    onChangeQuantity,
    onClickDelete,
  } = props;

  return (
    <Flex justify="space-between" align="center">
      <TicketProductMeta
        name={name}
        description={description}
        image={imageUrl}
      />

      <PriceTag price={price} currency={currency} />
    </Flex>
  );
};
